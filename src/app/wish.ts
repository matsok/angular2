export class Wish {
    id: number;
    itemId: number;
    itemName: string;
    userId: number;
    userName: string;
}
