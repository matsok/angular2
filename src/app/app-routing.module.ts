import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ItemDetailComponent }  from './item-detail.component';
import { LostItemDetailComponent }  from './lostitem-detail.component';


import { ItemsComponent }       from './items.component';
import { UsersComponent }       from './users.component';
import { WishesComponent }       from './wishes.component';
import { LostItemsComponent }       from './lostitems.component';

const routes: Routes = [
  { path: '', redirectTo: '/items', pathMatch: 'full' },
  { path: 'detail/:id', component: ItemDetailComponent },
  { path: 'lostdetail/:id', component: LostItemDetailComponent },
  { path: 'users',     component: UsersComponent },

  { path: 'items',     component: ItemsComponent },
  { path: 'wishes',     component: WishesComponent },
  { path: 'lostitems',     component: LostItemsComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
