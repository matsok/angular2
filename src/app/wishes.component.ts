import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Wish } from './wish';
import { WishService } from './wish.service';
import { User } from './user';
import { UserService } from './user.service';
import { History } from './history';
import { HistoryService } from './history.service';


import { RatingModule } from "ngx-rating";

@Component({
    selector: 'my-wishes',
    templateUrl: './wishes.component.html',
    styleUrls: ['./wishes.component.css']
})
export class WishesComponent implements OnInit {
    wishes: Wish[];
    selectedWish: Wish;
    showHistoryEnabled: boolean = false;
    history: History[];

    constructor(
        private wishService: WishService,
        private userService: UserService,
        private historyService: HistoryService,
        private router: Router) { }

    showHistory(): void {
        this.showHistoryEnabled = true;
    }

    getUsers(): void {
        this.wishService
            .getWishes()
            .then(data => this.wishes = data);
    }

    getHistory(): void {
        this.historyService
            .getHistories()
            .then(data => this.history = data);
    }

    ngOnInit(): void {
        this.getUsers();
        this.getHistory();
    }

    onSelect(wish: Wish): void {
        this.selectedWish = wish;
    }
}