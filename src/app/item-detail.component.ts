import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';

import { Item } from './item';
import { ItemService } from './item.service';

import { User } from './user';
import { UserService } from './user.service';

import { Wish } from './wish';
import { WishService } from './wish.service';

import { History } from './history';
import { HistoryService } from './history.service';

@Component({
  selector: 'item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {
  item: Item;
  items: Item[];
  imgSrc: string;
  addWishEnabled: boolean = false;
  users: User[];
  selectedUser: User;
  wishes: Wish[];
  userWishes: Wish[];
  histories: History[];
  wishesToRemove: Wish[];
  wishDuplicate: Wish[];

  constructor(
    private itemService: ItemService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private wishService: WishService,
    private historyService: HistoryService,
    private location: Location
  ) { }

  findWishesByItem(itemId: number): void {
    this.wishesToRemove = this.wishes.filter(function (value) {
      return (value.itemId === itemId);
    })
  }

  addWish(): void {
    if (this.isUserAbleToMakeWish(this.selectedUser.id) && this.isWishNotDuplicated(this.item.id, this.selectedUser.id)) {
      this.addToWishList(this.item.id, this.item.name, this.selectedUser.id, this.selectedUser.name);
      setTimeout(() => { this.giveToFirstPersonAfterTimeout(this.item.id); }, 10000);
    }
  }

  giveToFirstPersonAfterTimeout(itemId: number): void {
    this.findWishesByItem(itemId);
    if (this.wishesToRemove.length > 0) {
      this.addToHistory(this.wishesToRemove[0].itemId, this.wishesToRemove[0].itemName, this.wishesToRemove[0].userId, this.wishesToRemove[0].userName);
      for (let wishToRemove of this.wishesToRemove) {
        this.deleteAssociatedWishes(wishToRemove);
      }
    }
    this.itemService.getItem(itemId);
    this.deleteItem(this.item);
  }

  giveBackToOwner(): void {
    this.addToHistory(this.item.id, this.item.name, this.selectedUser.id, this.selectedUser.name);
    this.findWishesByItem(this.item.id);

    for (let wishToRemove of this.wishesToRemove) {
      this.deleteAssociatedWishes(wishToRemove);
    }

    this.deleteItem(this.item);
  }

  getUsers(): void {
    this.userService
      .getUsers()
      .then(data => this.users = data);
  }

  getHistories(): void {
    this.historyService
      .getHistories()
      .then(data => this.histories = data);
  }

  enableWishAdd(): void {
    this.addWishEnabled = true;
  }

  addToWishList(itemId: number, itemName: string, userId: number, userName: string): void {
    this.wishService.create(itemId, itemName, userId, userName)
      .then(wish => {
        this.wishes.push(wish);
      });
  }

  addToHistory(itemId: number, itemName: string, userId: number, userName: string): void {
    this.historyService.create(itemId, itemName, userId, userName)
      .then(history => {
        this.histories.push(history);
      });
  }

  filterWishesByUserId(userId: number): void {
    this.userWishes = this.wishes.filter(function (value) {
      return (value.userId === userId);
    })
  }

  isUserAbleToMakeWish(userId: number): boolean {
    this.filterWishesByUserId(userId);
    return this.userWishes.length < 3;
  }

  filterWishesByItemIdAndUserId(itemId: number, userId: number): void {
    this.wishDuplicate = this.wishes.filter(function (value) {
      return (value.itemId === itemId && value.userId === userId);
    })
  }

  isWishNotDuplicated(itemId: number, userId: number): boolean {
    this.filterWishesByItemIdAndUserId(itemId, userId);
    return this.wishDuplicate.length === 0;
  }

  deleteAssociatedWishes(wish: Wish): void {
    this.wishService
      .delete(wish.id)
      .then(() => {
        this.wishes = this.wishes.filter(h => h !== wish);
      });
  }

  deleteItem(item: Item): void {
    this.itemService
      .delete(item.id)
      .then(() => {
        this.items = this.items.filter(h => h !== item);
      });
  }

  getItems(): void {
    this.itemService
      .getItems()
      .then(items => this.items = items);
  }

  getWishes(): void {
    this.wishService
      .getWishes()
      .then(wishes => this.wishes = wishes);
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }

  ngOnInit(): void {
    this.getUsers();
    this.getWishes();
    this.getHistories();

    this.route.paramMap
      .switchMap((params: ParamMap) => this.itemService.getItem(+params.get('id')))
      .subscribe(data => this.item = data);
  }

  save(): void {
    this.itemService.update(this.item)
      .then(() => this.goBack());
  }

  goBack(): void {
    this.location.back();

  }
}
