import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { History } from './history';

@Injectable()
export class HistoryService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private historiesUrl = 'api/histories'; 

  constructor(private http: Http) { }

  getHistories(): Promise<History[]> {
    return this.http.get(this.historiesUrl)
               .toPromise()
               .then(response => response.json().data as History[])
               .catch(this.handleError);
  }

  getHistory(id: number): Promise<History> {
    const url = `${this.historiesUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as History)
      .catch(this.handleError);
  }

  create(itemId: number, itemName: string, userId: number, userName: string): Promise<History> {
    return this.http
      .post(this.historiesUrl, JSON.stringify({itemId: itemId, itemName: itemName, userId: userId, userName: userName}), {headers: this.headers})
      .toPromise()
      .then(res => res.json().data as History)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}

