import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const items = [
      { id: 0,  name: 'Teddy Bear', weight: 400, color:'pink', photo: 'https://rukminim1.flixcart.com/image/704/704/j0zlevk0/stuffed-toy/r/j/k/3-feet-teddy-bear-91-buy4babes-original-imaesy5dbagdgdca.jpeg?q=70', 
      category:  "toy", dateAdded: new Date(2016, 2, 3), rating: 1 },
      { id: 1, name: 'Panda', weight: 200, color:'white', photo: 'https://images-na.ssl-images-amazon.com/images/I/71GKX1Q9kaL._SY355_.jpg', 
      category:  "toy", dateAdded: new Date(2017, 2, 3), rating: 2 },
      { id: 2, name: 'Truck', weight: 3000, color:'red', photo: 'https://lh4.ggpht.com/n89YG-O3NQA0Qb9ppV1usmyMauXT3hFylse9lzi0ORe1vsAX3viRw7t8ZVNLChCtKQ=h310', 
      category:  "vehicle", dateAdded: new Date(2015, 2, 3), rating: 3 },
      { id: 3, name: 'Astra', weight: 1500, color:'silver', photo: 'http://media.dsm.co.za/images/Showcase/35814.jpg', 
      category:  "vehicle", dateAdded: new Date(2012, 2, 3), rating: 4 }
    ];

    const lostitems = [
      { id: 0,  name: 'Treasure box', weight: 5000, color:'brown', photo: 'https://images-na.ssl-images-amazon.com/images/I/9170pkv3BZL._SX355_.jpg', 
      category:  "toy", dateAdded: new Date(2016, 2, 3)},
      { id: 1, name: 'Message bottle', weight: 200, color:'none', photo: 'http://www.jujuhq.com/wp-content/uploads/2012/04/message-in-a-bottle.png', 
      category:  "toy", dateAdded: new Date(2017, 2, 3) },
      { id: 2, name: 'Key set', weight: 50, color:'silver', photo: 'https://thumbs.dreamstime.com/z/bunch-old-keys-rusty-cast-iron-ring-44441943.jpg', 
      category:  "luxury", dateAdded: new Date(2015, 2, 3) },
      { id: 3, name: 'Wedding ring', weight: 20, color:'silver', photo: 'https://i.pinimg.com/736x/a3/51/15/a35115d3d2e68bf3a826c5230f619535--wedding-goals-wedding-dreams.jpg', 
      category:  "luxury", dateAdded: new Date(2012, 2, 3) }
    ];

    const users = [
      { id: 0,  name: 'Mike', city: "Vancouver" },
      { id: 1, name: 'Michael', city: "Berlin" },
      { id: 2, name: 'Michal', city: "Warsaw" },
      { id: 3, name: 'Michel', city: "Paris" }
    ];

    const wishes = [
      { id: 0, itemId: 0, itemName: "Teddy Bear", userId: 0, userName: "Mike" },
      { id: 1, itemId: 0, itemName: "Teddy Bear", userId: 1, userName: "Michael" },
      { id: 2, itemId: 1, itemName: "Panda", userId: 0, userName: "Mike" }
    ];

    const histories = [
      { id: 0, itemId: 10, itemName: "Something", userId: 11, userName: "Someone" }
    ];

    return {items, users, wishes, histories, lostitems};
  }
}
