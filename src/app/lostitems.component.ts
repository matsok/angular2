import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LostItem } from './lostitem';
import { LostItemService } from './lostitem.service';

import { RatingModule } from "ngx-rating";

@Component({
  selector: 'my-lostitems',
  templateUrl: './lostitems.component.html',
  styleUrls: ['./lostitems.component.css']
})
export class LostItemsComponent implements OnInit {
  items: LostItem[];
  selectedItem: LostItem;
  newItemName: string;
  readDate: string;
  itemsFiltered: LostItem[];
  errorText: string;

  constructor(
    private lostItemService: LostItemService,
    private router: Router) { }

  getItems(): void {
    this.lostItemService
      .getItems()
      .then(data => this.items = data);
  }

  ngOnInit(): void {
    this.getItems();
  }

  setNewItemName(name: string): void {
    this.newItemName = name;
  }

  isInputFilled(name: string, weight: number, color: string, photo: string, category: string, readDate: string) {
    return name.length > 0
      && weight > 0
      && color.length > 0
      && photo.length > 0
      && category.length > 0
      && readDate.length > 0
      ;
  }

  epmtyErrorText(): void {
    this.errorText = undefined;
  }

  add(name: string, weight: number, color: string, photo: string, category: string): void {
    if (this.isInputFilled(name, weight, color, photo, category, this.readDate)) {
      name = name.trim();
      if (!name) { return; }
      this.lostItemService.create(name, weight, color, photo, category, this.readDate)
        .then(item => {
          this.items.push(item);
          this.selectedItem = null;
        });
      this.setNewItemName(name);
    }
    else {
      this.errorText = "Incorrect Data!";
      setTimeout(() => { this.epmtyErrorText(); }, 4000);
    }
  }

  filterByCategory(itemCategory: string): void {
    this.items = this.items.filter(function (value) {
      return (value.category === itemCategory);
    })
  }

  unfilterOrUnsort(): void {
    this.getItems();
  }

  sortByName(): void {
    this.items = this.items.sort(function (a, b) {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  }

  sortByWeight(): void {
    this.items = this.items.sort(function (a, b) {
      return a.weight - b.weight;
    }
    );
  }

  sortByDate(): void {
    this.items = this.items.sort(function (a, b) {
      return (new Date(b.dateAdded)).getTime() - (new Date(a.dateAdded)).getTime();
    }
    );
  }

  delete(item: LostItem): void {
    this.lostItemService
      .delete(item.id)
      .then(() => {
        this.items = this.items.filter(h => h !== item);
        if (this.selectedItem === item) { this.selectedItem = null; }
      });
  }

  onSelect(item: LostItem): void {
    this.selectedItem = item;
    console.log(this.items.length);
  }
}