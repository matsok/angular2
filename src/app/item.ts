export class Item {
    id: number;
    name: string;
    weight: number;
    color: string;
    photo: string;
    category: string;
    dateAdded: string;
    rating: number;
  }
  