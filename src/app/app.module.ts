import { NgModule, Pipe }           from '@angular/core';
import { BrowserModule }            from '@angular/platform-browser';
import { FormsModule }              from '@angular/forms';
import { HttpModule, JsonpModule }  from '@angular/http';

import { AppRoutingModule }     from './app-routing.module';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { AppComponent }         from './app.component';
import { ItemService }          from './item.service';
import { UserService }          from './user.service';
import { WishService }          from './wish.service';
import { HistoryService }       from './history.service';
import { LostItemService }      from './lostitem.service';

import { ItemsComponent }           from './items.component';
import { ItemDetailComponent }      from './item-detail.component';
import { UsersComponent }           from './users.component';
import { WishesComponent }          from './wishes.component';
import { LostItemsComponent }       from './lostitems.component';
import { LostItemDetailComponent }  from './lostitem-detail.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    ItemsComponent,
    LostItemsComponent,
    ItemDetailComponent,
    UsersComponent,
    WishesComponent,
    LostItemDetailComponent
  ],
  providers: [ 
    ItemService,
    LostItemService,
    UserService,
    WishService,
    HistoryService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
