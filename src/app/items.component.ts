import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Item } from './item';
import { ItemService } from './item.service';


@Component({
  selector: 'my-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  items: Item[];
  selectedItem: Item;
  newItemName: string;
  readDate: string;
  itemsFiltered: Item[];
  rating: number;
  errorText: string;

  constructor(
    private itemService: ItemService,
    private router: Router) { }


  onClickRating(rating: number): void {
    this.rating = rating;
  }

  getItems(): void {
    this.itemService
      .getItems()
      .then(data => this.items = data);
  }

  ngOnInit(): void {
    this.getItems();
  }

  setNewItemName(name: string): void {
    this.newItemName = name;
  }

  isInputFilled(name: string, weight: number, color: string, photo: string, category: string, readDate: string, rating: number) {
    return name.length > 0
      && weight > 0
      && color.length > 0
      && photo.length > 0
      && category.length > 0
      && readDate.length > 0
      && rating > 0
      ;
  }

  epmtyErrorText(): void {
    this.errorText = undefined;
  }

  add(name: string, weight: number, color: string, photo: string, category: string, rating: number): void {
    if (this.isInputFilled(name, weight, color, photo, category, this.readDate, this.rating)) {
      name = name.trim();
      if (!name) { return; }
      this.itemService.create(name, weight, color, photo, category, this.readDate, this.rating)
        .then(item => {
          this.items.push(item);
          this.selectedItem = null;
        });
      this.setNewItemName(name);
    }
    else {
      this.errorText = "Incorrect Data!";
      setTimeout(() => { this.epmtyErrorText(); }, 4000);
    }
    this.rating = 0;
  }

  filterByCategory(itemCategory: string): void {
    this.items = this.items.filter(function (value) {
      return (value.category === itemCategory);
    })
  }

  unfilterOrUnsort(): void {
    this.getItems();
  }

  sortByName(): void {
    this.items = this.items.sort(function (a, b) {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  }

  sortByWeight(): void {
    this.items = this.items.sort(function (a, b) {
      return a.weight - b.weight;
    }
    );
  }

  sortByDate(): void {
    this.items = this.items.sort(function (a, b) {
      return (new Date(b.dateAdded)).getTime() - (new Date(a.dateAdded)).getTime();
    }
    );
  }

  delete(item: Item): void {
    this.itemService
      .delete(item.id)
      .then(() => {
        this.items = this.items.filter(h => h !== item);
        if (this.selectedItem === item) { this.selectedItem = null; }
      });
  }

  onSelect(item: Item): void {
    this.selectedItem = item;
    console.log(this.items.length);
  }
}