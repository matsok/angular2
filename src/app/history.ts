export class History {
    id: number;
    itemId: number;
    itemName: string;
    userId: number;
    userName: string;
}
