import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { LostItem } from './lostitem';

@Injectable()
export class LostItemService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private itemsUrl = 'api/lostitems';

  constructor(private http: Http) { }

  getItems(): Promise<LostItem[]> {
    return this.http.get(this.itemsUrl)
      .toPromise()
      .then(response => response.json().data as LostItem[])
      .catch(this.handleError);
  }

  getItem(id: number): Promise<LostItem> {
    const url = `${this.itemsUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json().data as LostItem)
      .catch(this.handleError);
  }

  delete(id: number): Promise<void> {
    const url = `${this.itemsUrl}/${id}`;
    return this.http.delete(url, { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  create(name: string, weight: number, color: string, photo: string, category: string, dateAdded: string): Promise<LostItem> {
    return this.http
      .post(this.itemsUrl, JSON.stringify({ name: name, weight: weight, color: color, photo: photo, category: category, dateAdded: dateAdded }), { headers: this.headers })
      .toPromise()
      .then(res => res.json().data as LostItem)
      .catch(this.handleError);
  }

  update(item: LostItem): Promise<LostItem> {
    const url = `${this.itemsUrl}/${item.id}`;
    return this.http
      .put(url, JSON.stringify(item), { headers: this.headers })
      .toPromise()
      .then(() => item)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
