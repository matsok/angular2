export class LostItem {
  id: number;
  name: string;
  weight: number;
  color: string;
  photo: string;
  category: string;
  dateAdded: string;
}
